from aiogram.utils import executor
from loguru import logger

from loader import dp


async def on_startup(_):
    logger.info('Bot started')


async def on_shutdown(_):
    logger.info('Bot stopped')


if __name__ == '__main__':
    import handlers

    executor.start_polling(
        dp,
        skip_updates=True,
        on_startup=on_startup,
        on_shutdown=on_shutdown
    )
