from aiogram import types

from filters import IsMe
from loader import dp


@dp.message_handler(IsMe())
async def echo_bot(message: types.Message):
    await message.answer(message.text)
