from aiogram.types import InlineKeyboardMarkup, InlineKeyboardButton

from keyboards.inline_kbs.callback_datas import start_callback_data

start_inline_kb = InlineKeyboardMarkup(inline_keyboard=[
    [
        InlineKeyboardButton('Hello', callback_data=start_callback_data.new(index=1))  # start:1
    ]
])
