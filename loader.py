from configparser import ConfigParser

from aiogram import Bot, Dispatcher
from aiogram.contrib.fsm_storage.memory import MemoryStorage

config = ConfigParser()
config.read('config.ini')

bot = Bot(
    token=config['Telegram']['token'],
    parse_mode='html'
)

storage = MemoryStorage()

dp = Dispatcher(
    bot=bot,
    storage=storage
)
