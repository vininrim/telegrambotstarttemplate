from aiogram import types
from aiogram.dispatcher.filters import Filter


class IsMe(Filter):
    key = "is_me"

    async def check(self, message: types.Message):
        return True if message.from_user.first_name == 'vanya' else False
