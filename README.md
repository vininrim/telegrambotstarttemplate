# TelegramBotStartTemplate

Стартовый шаблон для Telegram ботов на aiogram

<h2>Getting started</h2>
<p>

После установки зависимостей, в корне проекта (в дирректории с файлом `bot.py`) необходимо создать файл `config.ini` и заполнить его

    [Telegram]
    token = your_bot_token

Для запуска бота необходимо запускать файл `bot.py`

</p>
